//This code and idea is my own. The majority of the main method is from class/book with small changes within it
#include<stdio.h>
#include<stdlib.h>

int used = 0;
int available = 1;
int mutex = 1;
int x = 0;

int wait(int i)
{
	return i-1;
}
int signal(int i)
{
	return i+1;
}
int produce()
{
	mutex = wait(mutex);
	used = signal(used);
	available = wait(available);
	x++;
	printf("Sorry, but there are %d computers currently being used in the library\n", used);
	mutex = signal(mutex);


}
int consume()
{
	mutex = wait(mutex);
	used = wait(used);
	available = signal(available);
	x--;
	printf("There are %d computers available in the library\n", available);
	mutex = signal(mutex);
}

int main()
{
	int opt = 0; 
	//infinite loop
	while(1)
	{
		printf("Please select an option: \n 1. Use a Computer\n 2. Leave a Computer \n 3. Exit");
		scanf("%d", &opt);
		//switch statement depending on the choice from the user
		switch(opt)
		{
			case 1:
			
			printf("Use a Computer has been chosen\n");
			
			if(mutex == 1 && available !=0)
			{
				produce();
			
			}
			else
			{
				printf("Computers are all ready being used, sorry.\n");
			}
			break;
			case 2:
			
			printf("Leave a Computer has been chosen\n");
			
			if(mutex == 1 && used !=0)
			{
				consume();
			
			}
			else
			{
				printf("No Computers are being used.\n");
			}
			break;
			case 3:
			
			printf("Exit was chosen\n");
			exit(0);
			break;	
		}	
	}
}
